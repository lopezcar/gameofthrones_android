package es.npatarino.android.gotchallenge.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import es.npatarino.android.gotchallenge.model.GoTCharacter;
import es.npatarino.android.gotchallenge.R;
import es.npatarino.android.gotchallenge.adapters.GoTAdapter;
import es.npatarino.android.gotchallenge.customs.Utils;
import es.npatarino.android.gotchallenge.interfaces.ListFragment;

/**
 * Created by antoniohormigo on 4/3/16.
 */
public class GoTListFragment extends Fragment implements ListFragment {

    public static final String TAG = "GoTListFragment";

    private GoTAdapter adp;
    private static String houseId;

    public GoTListFragment() {
    }

    public static GoTListFragment newInstance(String id) {
        houseId = id;

        return new GoTListFragment();
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list, container, false);
        final ContentLoadingProgressBar pb = (ContentLoadingProgressBar) rootView.findViewById(R.id.pb);
        RecyclerView rv = (RecyclerView) rootView.findViewById(R.id.rv);

        adp = new GoTAdapter(getActivity());
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv.setHasFixedSize(true);
        rv.setAdapter(adp);

        new Thread(new Runnable() {

            @Override
            public void run() {

                try {
                    String stringJson = Utils.getJsonString(getActivity(), getString(R.string.url));

                    Type listType = new TypeToken<ArrayList<GoTCharacter>>() {
                    }.getType();

                    final List<GoTCharacter> finalChars = new ArrayList<GoTCharacter>();
                    if (stringJson != null) {
                        final List<GoTCharacter> characters = new Gson().fromJson(stringJson, listType);

                        if (houseId != null) {
                            for (GoTCharacter charac : characters) {
                                if (charac.getHi().equals(houseId)) {
                                    finalChars.add(charac);
                                }
                            }
                        } else if (characters != null) {
                            finalChars.addAll(characters);
                        }
                    }
                    GoTListFragment.this.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adp.addAll(finalChars);
                            adp.notifyDataSetChanged();
                            pb.hide();
                        }
                    });
                } catch (IOException e) {
                    Log.e(TAG, e.getLocalizedMessage());
                }
            }
        }).start();
        return rootView;
    }

    @Override
    public void applyFilter(String text) {
        adp.filter(text);
        adp.notifyDataSetChanged();
    }

    @Override
    public void cancelFilter() {
        adp.removeFilter();
        adp.notifyDataSetChanged();
    }

    @Override
    public Fragment getFragment() {
        return this;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        houseId = null;
    }
}

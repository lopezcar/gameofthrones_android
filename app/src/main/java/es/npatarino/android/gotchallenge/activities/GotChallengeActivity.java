package es.npatarino.android.gotchallenge.activities;

import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import es.npatarino.android.gotchallenge.R;
import es.npatarino.android.gotchallenge.interfaces.OnSearchListener;

/**
 * Created by antoniohormigo on 4/3/16.
 */
public class GotChallengeActivity extends AppCompatActivity {

    private boolean toolBarIsSearcher = false;

    Toolbar mToolbar;
    Toolbar mToolbarSearch;
    private MenuItem mItemClose;

    private OnSearchListener mOnSearchListener;

    EditText mEditTextFromToolbar;
    ImageView mActionBack;
    LinearLayout mLayoutSearcher;

    protected void initToolbar(Toolbar toolbar) {

        mToolbar = toolbar;

        // Set the toolbar
        setSupportActionBar(toolbar);
    }

    protected void initToolbarSearcher(Toolbar toolbar, OnSearchListener listener) {

        mToolbarSearch = toolbar;

        mOnSearchListener = listener;

        mEditTextFromToolbar = (EditText) mToolbarSearch.findViewById(R.id.et_searcher);
        mActionBack = (ImageView) mToolbarSearch.findViewById(R.id.backButton);
        mLayoutSearcher = (LinearLayout) mToolbarSearch.findViewById(R.id.ll_arrow_searcher);

        mActionBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                showToolbar();
            }
        });

        mEditTextFromToolbar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String busqueda = mEditTextFromToolbar.getText().toString();

                mOnSearchListener.onSearch(busqueda);

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mEditTextFromToolbar.getWindowToken(), 0);

                return true;
            }


        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (toolBarIsSearcher) {
            getMenuInflater().inflate(R.menu.menu_searcher, menu);
            mItemClose = menu.findItem(R.id.action_cancel);
        } else {
            getMenuInflater().inflate(R.menu.menu_home, menu);
        }

        return true;
    }

    public void showToolbar() {
        toolBarIsSearcher = false;
        setSupportActionBar(mToolbar);
        mToolbar.setVisibility(View.VISIBLE);
        if (mToolbarSearch != null) {
            mToolbarSearch.setVisibility(View.GONE);
            toolBarIsSearcher = false;
            mOnSearchListener.onCancel();
        }
    }

    private void setAnimationToSearcherBar() {

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            display.getSize(size);
        }
        final int width = size.x;

        TranslateAnimation anim = new TranslateAnimation(width, 0, 0, 0);
        anim.setDuration(700);

        anim.setAnimationListener(new TranslateAnimation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                mItemClose.setVisible(false);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mItemClose.setVisible(true);
            }
        });


        mLayoutSearcher.startAnimation(anim);
    }

    public void showSearcherToolbar() {
        if (mToolbarSearch != null) {
            mToolbar.setVisibility(View.GONE);
            mToolbarSearch.setVisibility(View.VISIBLE);
            //mOnSearchListener.onInit();

            if (!toolBarIsSearcher) {
                mEditTextFromToolbar.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(mEditTextFromToolbar, InputMethodManager.SHOW_IMPLICIT);
            }
            toolBarIsSearcher = true;
            setSupportActionBar(mToolbarSearch);
        } else {
            Log.d("GotChallenge", "Toolbar search not initialized");
        }

    }

    @Override
    public void onBackPressed() {
        if (toolBarIsSearcher) {
            if (mToolbar != null) mToolbar.setVisibility(View.VISIBLE);
            if (mToolbarSearch != null) {
                mToolbarSearch.setVisibility(View.GONE);
                toolBarIsSearcher = false;
                setSupportActionBar(mToolbarSearch);
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:

                showSearcherToolbar();
                setAnimationToSearcherBar();

                return true;

            case R.id.action_cancel:

                if (mEditTextFromToolbar.getText().toString().equals("")) {
                    showToolbar();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);

                } else {
                    mEditTextFromToolbar.setText("");
                }

                //NavigationHelper.
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

}

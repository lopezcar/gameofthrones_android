package es.npatarino.android.gotchallenge.presenters;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import es.npatarino.android.gotchallenge.fragments.GoTListFragment;
import es.npatarino.android.gotchallenge.interfaces.ListActivity;
import es.npatarino.android.gotchallenge.interfaces.ListFragment;
import es.npatarino.android.gotchallenge.interfaces.ListPresenter;

/**
 * Created by antoniohormigo on 5/3/16.
 */
public class CharListPresenter implements ListPresenter {

    Context mContext;
    FrameLayout frameLayout;
    private ListFragment mFragment;
    private ListActivity mView;

    public CharListPresenter(Context context, ListActivity view) {
        mView = view;
        mContext = context;
    }

    @Override
    public void onElementClicked(String houseId) {
        final FragmentManager fragmentManager = mView.getSupportFragMngr();
        final FragmentTransaction transaction = fragmentManager.beginTransaction();
        mFragment = GoTListFragment.newInstance(houseId);

        transaction.replace(frameLayout.getId(), mFragment.getFragment(), GoTListFragment.TAG);

        transaction.commit();
    }

    @Override
    public void setContainer(ViewGroup vg) {
        this.frameLayout = (FrameLayout) vg;
    }

    @Override
    public void applyFilter(String text) {
        mFragment.applyFilter(text);
    }

    @Override
    public void cancelFilter() {
        mFragment.cancelFilter();
    }
}

package es.npatarino.android.gotchallenge.interfaces;

import android.support.v4.app.Fragment;

/**
 * Created by antoniohormigo on 5/3/16.
 */
public interface ListFragment {

    void applyFilter(String text);

    void cancelFilter();

    Fragment getFragment();

}

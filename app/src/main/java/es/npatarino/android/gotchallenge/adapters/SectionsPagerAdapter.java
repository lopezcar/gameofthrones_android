package es.npatarino.android.gotchallenge.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import es.npatarino.android.gotchallenge.R;
import es.npatarino.android.gotchallenge.fragments.GoTHousesListFragment;
import es.npatarino.android.gotchallenge.fragments.GoTListFragment;
import es.npatarino.android.gotchallenge.interfaces.ListFragment;

/**
 * Created by antoniohormigo on 4/3/16.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    private ListFragment mCharsFragment;
    private ListFragment mHousesFragment;

    private Context ctx;

    public SectionsPagerAdapter(Context ctx, FragmentManager fm) {
        super(fm);

        this.ctx = ctx;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            mCharsFragment = new GoTListFragment();
            return mCharsFragment.getFragment();
        } else {
            mHousesFragment = new GoTHousesListFragment();
            return mHousesFragment.getFragment();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return ctx.getString(R.string.chars);
            case 1:
                return ctx.getString(R.string.houses);
        }
        return null;
    }

    public void applyFilter(String text) {
        mCharsFragment.applyFilter(text);
    }

    public void cancelFilter() {
        mCharsFragment.cancelFilter();
    }
}

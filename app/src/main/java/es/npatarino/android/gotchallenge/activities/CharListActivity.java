package es.npatarino.android.gotchallenge.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;

import es.npatarino.android.gotchallenge.R;
import es.npatarino.android.gotchallenge.interfaces.ListActivity;
import es.npatarino.android.gotchallenge.interfaces.ListPresenter;
import es.npatarino.android.gotchallenge.interfaces.OnSearchListener;
import es.npatarino.android.gotchallenge.presenters.CharListPresenter;

public class CharListActivity extends GotChallengeActivity implements ListActivity {

    Toolbar toolbar;
    Toolbar toolbarSearch;
    ListPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_char_list);

        presenter = new CharListPresenter(this, this);

        final String houseName = getIntent().getStringExtra("houseName");
        final String houseId = getIntent().getStringExtra("houseId");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(houseName);
        initToolbar(toolbar);

        presenter.setContainer((FrameLayout) findViewById(R.id.container));

        // toolbar search
        toolbarSearch = (Toolbar) findViewById(R.id.toolbarSearcher);

        presenter.onElementClicked(houseId);

        initToolbarSearcher(toolbarSearch, new OnSearchListener() {
            @Override
            public void onSearch(String textToSearch) {
                presenter.applyFilter(textToSearch);
            }

            @Override
            public void onCancel() {
                presenter.cancelFilter();
            }
        });
    }

    @Override
    public FragmentManager getSupportFragMngr() {
        return getSupportFragmentManager();
    }
}

package es.npatarino.android.gotchallenge.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import es.npatarino.android.gotchallenge.activities.DetailActivity;
import es.npatarino.android.gotchallenge.model.GoTCharacter;
import es.npatarino.android.gotchallenge.R;

/**
 * Created by antoniohormigo on 4/3/16.
 */
public class GoTAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private final List<GoTCharacter> gcs;
    private List<GoTCharacter> filteredGcs;
    private Activity a;

    public GoTAdapter(Activity activity) {
        this.gcs = new ArrayList<>();
        this.filteredGcs = null;
        a = activity;
    }

    public void addAll(Collection<GoTCharacter> collection) {
        for (int i = 0; i < collection.size(); i++) {
            gcs.add((GoTCharacter) collection.toArray()[i]);
        }
    }

    public void filter(String filterText) {
        this.filteredGcs = new ArrayList<>();
        for (int i = 0; i < gcs.size(); i++) {
            if (gcs.get(i).getN() != null
                    && gcs.get(i).getN().toUpperCase().contains(
                    filterText.toUpperCase())) {
                filteredGcs.add(gcs.get(i));
            }
        }
    }

    public void removeFilter() {
        filteredGcs = null;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GotCharacterViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.got_character_row, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        GotCharacterViewHolder gotCharacterViewHolder = (GotCharacterViewHolder) holder;

        if (filteredGcs != null) {
            gotCharacterViewHolder.render(filteredGcs.get(position));
        } else {
            gotCharacterViewHolder.render(gcs.get(position));
        }

        ((GotCharacterViewHolder) holder).imp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Intent intent = new Intent(((GotCharacterViewHolder) holder).itemView.getContext(), DetailActivity.class);
                intent.putExtra("description", gcs.get(position).getD());
                intent.putExtra("name", gcs.get(position).getN());
                intent.putExtra("imageUrl", gcs.get(position).getIu());
                ((GotCharacterViewHolder) holder).itemView.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (filteredGcs != null) {
            return filteredGcs.size();
        } else {
            return gcs.size();
        }
    }

    class GotCharacterViewHolder extends RecyclerView.ViewHolder {

        private static final String TAG = "GotCharacterViewHolder";
        ImageView imp;
        TextView tvn;

        public GotCharacterViewHolder(View itemView) {
            super(itemView);
            imp = (ImageView) itemView.findViewById(R.id.ivBackground);
            tvn = (TextView) itemView.findViewById(R.id.tv_name);
        }

        public void render(final GoTCharacter goTCharacter) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    a.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Picasso.with(a).load(goTCharacter.getIu()).placeholder(
                                    R.color.backgroundColor).into(imp);
                            tvn.setText(goTCharacter.getN());
                        }
                    });

                }
            }).start();
        }
    }

}

package es.npatarino.android.gotchallenge.interfaces;

import android.view.ViewGroup;

/**
 * Created by antoniohormigo on 5/3/16.
 */
public interface ListPresenter {

    void onElementClicked(String id);

    void setContainer(ViewGroup vg);

    void applyFilter(String text);

    void cancelFilter();

}
